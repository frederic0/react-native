import * as React from 'react';
import { Component } from 'react-simplified';
import { View, Text, TextInput } from 'react-native';
import moji from 'moji-translate';

class Card extends Component<{ title?: string }> {
  render() {
    return (
      <View
        style={{
          borderRadius: 6,
          backgroundColor: '#fff',
          shadowOffset: { width: 1, height: 1 },
          shadowOpacity: 0.3,
          shadowRadius: 4,
          margin: 5,
        }}
      >
        <Text style={{ margin: 10, fontSize: 24 }}>{this.props.title}</Text>
        <View style={{ margin: 10 }}>{this.props.children}</View>
      </View>
    );
  }
}

export class App extends Component {
  input = '';


  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center'}}>
        <Text style={{fontSize: 50}}>{"Type something :)\n"}{moji.translate(this.input)}</Text>
        <TextInput
          style={{ borderWidth: 1, margin: 20, fontSize: 50 }}
          value={this.input}
          onChangeText={(text) => (this.input = text)}
        />
      </View>
      
    );
  }
}
